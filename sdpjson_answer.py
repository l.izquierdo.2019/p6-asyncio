import asyncio
import json
import sdp_transform


class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        data_dict = json.loads(data)
        sdp_dict = sdp_transform.parse(data_dict['sdp'])
        for i in sdp_dict['media']:
            i['port'] = 34543
        sdp = sdp_transform.write(sdp_dict)
        data = {
            'type': "answer",
            'sdp': sdp
        }
        a = json.dumps(data).encode()
        print('Received %r from %s' % (sdp, addr))
        print('Send %r to %s' % (sdp, addr))
        self.transport.sendto(a, addr)


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.0', 9999))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()


asyncio.run(main())